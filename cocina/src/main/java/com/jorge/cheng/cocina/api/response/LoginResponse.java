package com.jorge.cheng.cocina.api.response;

import com.jorge.cheng.cocina.db.entities.User;

public class LoginResponse {
    private String message;
    private User data;

    public String getMessage() {
        return message;
    }

    public User getData() {
        return data;
    }
}
