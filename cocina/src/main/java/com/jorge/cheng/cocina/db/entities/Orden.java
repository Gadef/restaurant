package com.jorge.cheng.cocina.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Orden {
    @NonNull
    @PrimaryKey
    private  String fechaRegistro;
    private  String cliente_id;
    private  String _id;
    private  double total;
    private  double __v;

    @NonNull
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(@NonNull String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(String cliente_id) {
        this.cliente_id = cliente_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double get__v() {
        return __v;
    }

    public void set__v(double __v) {
        this.__v = __v;
    }
}
