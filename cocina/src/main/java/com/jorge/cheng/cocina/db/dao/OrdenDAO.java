package com.jorge.cheng.cocina.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.jorge.cheng.cocina.db.entities.DetalleOrden;
import com.jorge.cheng.cocina.db.entities.Orden;
import com.jorge.cheng.cocina.db.views.OrdenCliente;

import java.util.List;

@Dao
public interface OrdenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Void insert(List<Orden> ordenList);

    @Query("SELECT * FROM orden")
    List<Orden> getAll();

    @Query("SELECT * FROM ordencliente")
    LiveData<List<OrdenCliente>> getOrdenClienteAll();


}
