package com.jorge.cheng.cocina.ui.ordenes;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.jorge.cheng.cocina.api.ApiManager;
import com.jorge.cheng.cocina.api.response.OrdenesResponse;
import com.jorge.cheng.cocina.db.CocinaRoomDatabase;
import com.jorge.cheng.cocina.db.entities.Cliente;
import com.jorge.cheng.cocina.db.entities.DetalleOrden;
import com.jorge.cheng.cocina.db.entities.Orden;
import com.jorge.cheng.cocina.db.views.OrdenCliente;
import com.jorge.cheng.cocina.util.AppExecutors;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdenesViewModel extends AndroidViewModel {

    private CocinaRoomDatabase database;
    private LiveData<List<OrdenCliente>> ordenes;
    private AppExecutors executors;
    private MutableLiveData<Boolean> loading;

    public OrdenesViewModel(@NonNull Application application) {
        super(application);
        database = CocinaRoomDatabase.getDatabase(application);
        ordenes = database.ordenDAO().getOrdenClienteAll();
        executors = new AppExecutors();
        loading = new MutableLiveData<>();
        loading.setValue(false);
        getOrdenesFromService();
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public LiveData<List<OrdenCliente>> getOrdenes() {
        return ordenes;
    }

    private void getOrdenesFromService() {
        loading.setValue(true);
        ApiManager.getApiManagerInstance().getOrdenes().enqueue(new Callback<OrdenesResponse>() {
            @Override
            public void onResponse(Call<OrdenesResponse> call, Response<OrdenesResponse> response) {
                if (response.isSuccessful()) {
                    executors.diskIO().execute(() -> {
                        List<Cliente> clienteList = new ArrayList<>();
                        List<Orden> ordenList = new ArrayList<>();
                        List<DetalleOrden> detalleOrdenList = new ArrayList<>();
                        for (OrdenesResponse.OrdenRetrofit data : response.body().getData()) {
                            ordenList.add(getOrdenFromRetrofit(data));
                            clienteList.add(getClienteFromRetrofit(data.getCliente_id()));
                            for (OrdenesResponse.DetalleOrdenRetrofit detalleOrdenRetrofit : data.getDetalle()) {
                                detalleOrdenList.add(getDetalleOrdenFromRetrofit(data, detalleOrdenRetrofit));
                            }
                        }
                        database.clienteDAO().insert(clienteList);
                        database.ordenDAO().insert(ordenList);
                        database.detalleOrdenDAO().insert(detalleOrdenList);
                        loading.postValue(false);
                    });
                } else {
                    loading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<OrdenesResponse> call, Throwable t) {
                loading.setValue(false);
                Log.w("TAG", "onFailure: ", t);
            }
        });
    }

    private DetalleOrden getDetalleOrdenFromRetrofit(OrdenesResponse.OrdenRetrofit data, OrdenesResponse.DetalleOrdenRetrofit detalleOrdenRetrofit) {
        DetalleOrden detalleOrden = new DetalleOrden();
        detalleOrden.set_id(detalleOrdenRetrofit.get_id());
        detalleOrden.setOrden_id(data.get_id());
        detalleOrden.setPlato_id(detalleOrdenRetrofit.getPlato_id());
        detalleOrden.setNombre(detalleOrdenRetrofit.getNombre());
        detalleOrden.setPrecio(detalleOrdenRetrofit.getPrecio());
        detalleOrden.setCantidad(detalleOrdenRetrofit.getCantidad());
        return detalleOrden;
    }

    private Cliente getClienteFromRetrofit(OrdenesResponse.ClienteRetrofit data) {
        Cliente cliente = new Cliente();
        cliente.set_id(data.get_id());
        cliente.setImagen(data.getImagen());
        cliente.setNombres(data.getNombres());
        cliente.setApellido_paterno(data.getApellido_paterno());
        cliente.setApellido_materno(data.getApellido_materno());
        return cliente;
    }

    private Orden getOrdenFromRetrofit(OrdenesResponse.OrdenRetrofit data) {
        Orden orden = new Orden();
        orden.set_id(data.get_id());
        orden.setFechaRegistro(data.getFechaRegistro());
        orden.setCliente_id(data.getCliente_id().get_id());
        orden.setTotal(data.getTotal());
        orden.set__v(data.get__v());
        return orden;
    }

}

