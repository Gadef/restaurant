package com.jorge.cheng.cocina.api;

class ApiConstants {
    static final int TIMEOUT = 60;
    static final String BASE_URL = "https://diplomado-restaurant-backend.herokuapp.com/";
    static final String LOGIN = "auth/usuarios-login";
    static final String GET_ORDENES = "ordenes-de-compra";

}
