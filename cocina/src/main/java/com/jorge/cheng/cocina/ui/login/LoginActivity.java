package com.jorge.cheng.cocina.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jorge.cheng.cocina.R;
import com.jorge.cheng.cocina.api.ApiManager;
import com.jorge.cheng.cocina.api.request.LoginRequest;
import com.jorge.cheng.cocina.api.response.LoginResponse;
import com.jorge.cheng.cocina.db.CocinaRoomDatabase;
import com.jorge.cheng.cocina.util.AppExecutors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText etEmail;
    private EditText etPassword;
    private CocinaRoomDatabase database;
    private AppExecutors executors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        database = CocinaRoomDatabase.getDatabase(this);
        executors = new AppExecutors();

        etEmail= findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    login();
                    return true;
                }
                return false;
            }
        });

    }

    private void login() {
        LoginRequest request = new LoginRequest();
        request.setEmail(etEmail.getText().toString());
        request.setPassword(etPassword.getText().toString());

        ApiManager.getApiManagerInstance().login(request).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        database.userDAO().insert(response.body().getData());
                        executors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, response.body().toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }
}
