package com.jorge.cheng.cocina.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.jorge.cheng.cocina.db.entities.User;

import java.util.List;

@Dao
public interface UserDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Void insert(User user);

    @Query("SELECT * FROM user")
    List<User> getAll();

}
