package com.jorge.cheng.cocina.ui.ordenes;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jorge.cheng.cocina.R;
import com.jorge.cheng.cocina.api.ApiManager;
import com.jorge.cheng.cocina.api.response.OrdenesResponse;
import com.jorge.cheng.cocina.db.CocinaRoomDatabase;
import com.jorge.cheng.cocina.db.entities.Cliente;
import com.jorge.cheng.cocina.db.entities.DetalleOrden;
import com.jorge.cheng.cocina.db.entities.Orden;
import com.jorge.cheng.cocina.ui.base.BaseFragment;
import com.jorge.cheng.cocina.util.AppExecutors;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A placeholder fragment containing a simple view.
 */
public class OrdenesFragment extends BaseFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private int index;
    private OrdenesViewModel viewModel;

    public static OrdenesFragment newInstance(int index) {
        OrdenesFragment fragment = new OrdenesFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ordenes, container, false);
        RvAdapter adapter = new RvAdapter();
        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        viewModel = ViewModelProviders.of(this).get(OrdenesViewModel.class);
        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean){
                    showProgressDialog();
                }else {
                    hideProgressDialog();
                }
            }
        });
        return root;
    }
}

