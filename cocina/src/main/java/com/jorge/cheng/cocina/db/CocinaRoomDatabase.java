package com.jorge.cheng.cocina.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.jorge.cheng.cocina.db.dao.ClienteDAO;
import com.jorge.cheng.cocina.db.dao.DetalleOrdenDAO;
import com.jorge.cheng.cocina.db.dao.OrdenDAO;
import com.jorge.cheng.cocina.db.dao.UserDAO;
import com.jorge.cheng.cocina.db.entities.Cliente;
import com.jorge.cheng.cocina.db.entities.DetalleOrden;
import com.jorge.cheng.cocina.db.entities.Orden;
import com.jorge.cheng.cocina.db.entities.User;
import com.jorge.cheng.cocina.db.views.OrdenCliente;

@Database(entities = {User.class, Cliente.class, DetalleOrden.class, Orden.class},views = {OrdenCliente.class},version = 3, exportSchema = false)
public abstract class CocinaRoomDatabase extends RoomDatabase {

    private static volatile CocinaRoomDatabase INSTANCE;

    public static CocinaRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CocinaRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room
                            .databaseBuilder(context.getApplicationContext(), CocinaRoomDatabase.class, "cocina_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDAO userDAO();
    public abstract ClienteDAO clienteDAO();
    public abstract DetalleOrdenDAO detalleOrdenDAO();
    public abstract OrdenDAO ordenDAO();

}

