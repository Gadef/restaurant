package com.jorge.cheng.cocina.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.jorge.cheng.cocina.db.entities.Cliente;

import java.util.List;

@Dao
public interface ClienteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Void insert(List<Cliente> clienteList);

    @Query("SELECT * FROM cliente")
    List<Cliente> getAll();

}
