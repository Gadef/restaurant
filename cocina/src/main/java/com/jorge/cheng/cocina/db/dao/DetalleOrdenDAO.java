package com.jorge.cheng.cocina.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.jorge.cheng.cocina.db.entities.DetalleOrden;

import java.util.List;

@Dao
public interface DetalleOrdenDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Void insert(List<DetalleOrden> detalleOrdenList);

    @Query("SELECT * FROM detalleOrden")
    List<DetalleOrden> getAll();

}
