package com.jorge.cheng.cocina.api;

import com.jorge.cheng.cocina.api.request.LoginRequest;
import com.jorge.cheng.cocina.api.response.LoginResponse;
import com.jorge.cheng.cocina.api.response.OrdenesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {
    @POST(ApiConstants.LOGIN)
    Call<LoginResponse> login(@Body LoginRequest request);

    @GET(ApiConstants.GET_ORDENES)
    Call<OrdenesResponse> getOrdenes();
}
