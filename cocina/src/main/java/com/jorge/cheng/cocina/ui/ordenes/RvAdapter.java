package com.jorge.cheng.cocina.ui.ordenes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jorge.cheng.cocina.R;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ViewHolder> {

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orden, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText("Nombre " + position);
        holder.tvMesa.setText("Mesa " + position);
        holder.tvTiempo.setText(position + "s");
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvMesa,tvTiempo;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvMesa = itemView.findViewById(R.id.tvMesa);
            tvTiempo = itemView.findViewById(R.id.tvTiempo);
        }
    }
}
